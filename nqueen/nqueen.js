let numberOfIteration = 0
let finalTemperature

function randomQueens(n) {
    let Arrayofquenns = []
    for (let i = 0; i < n; i++) {
        Arrayofquenns[i] = Math.floor((Math.random() * n));
    }
    return Arrayofquenns;
}

function conflictQueensNumber(queens_position, n) {
    let conflict = 0;
    for (let i = 0; i < n; i++) {
        for (let j = i + 1; j < n; j++) {
            if (queens_position[i] == queens_position[j] || Math.abs(queens_position[i] - queens_position[j]) == j - i) {
                conflict += 1;
            }
        }
    }
    return conflict;
}


function simulatedAnnealing(n, maxNumOfIterations, temperature, coolingFactor) {

    let posision_of_queens = randomQueens(n); 


    let conflictNumber = conflictQueensNumber(posision_of_queens, n);


    while (temperature > 0.0001) {
        for (let i = 0; i < maxNumOfIterations; i++) {
            queenMovement(posision_of_queens, conflictNumber, temperature, n);
            conflictNumber = conflictQueensNumber(posision_of_queens, n);
            numberOfIteration++;
            if (conflictNumber == 0) {
                finalTemperature = temperature;
                return posision_of_queens; 
            }
        }
      temperature=schedule(temperature,coolingFactor)
    }
    return null; 
}

function schedule(temp,coolingFactor) {
    temp -= temp * coolingFactor;
    return temp
}


function queenMovement(quennsPosition, targetCost, temp, n) {
    while (true) {

        let randomCol = Math.floor((Math.random() * n));
        let randomRow = Math.floor((Math.random() * n));

        let tmpRow = quennsPosition[randomCol];
        quennsPosition[randomCol] = randomRow;

        let cost = conflictQueensNumber(quennsPosition, n);

        if (cost < targetCost) {
            break;
        }

        else {
            let deltaEnergy = targetCost - cost;
            let acceptProb = Math.exp(deltaEnergy / temp);
            console.log(acceptProb)
            if (Math.random() < acceptProb) {
                break;
            }
            else {
                quennsPosition[randomCol] = tmpRow;
            }

        }

    }
}

let max_iteration = 1000000000;
let coolingFactor = 0.15;
let n = 8;
let log_of_E75 = 0.2
let startTemp = 1;

let result = simulatedAnnealing(n, max_iteration, startTemp, coolingFactor);
//console.log(result)
let data = JSON.stringify(result)

rerult = result.toString()
print(result, n)

function print(rs, n) {
    let printin = []
    for (let i = 0; i < n; i++) {
        for (let j = 0; j < n; j++)
            printin[i] = new Array(n);
    }

    for (let i = 0; i < printin.length; i++) {
        for (let j = 0; j < printin[0].length; j++) {
            printin[i][j] = ' ';
        }
    }
    for (let i = 0; i < n; i++) {
        printin[i][rs[i]] = "Q"
    }

    for (let i = 0; i < n; i++) {
        printin[i]
        printin = printin

    }
    console.table(printin)

}


